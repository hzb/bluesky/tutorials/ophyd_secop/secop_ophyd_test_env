FROM python:3.11

COPY requirements.txt . 
RUN python3 -m pip install -r requirements.txt

WORKDIR /opt
COPY cryo_demo.ipynb . 