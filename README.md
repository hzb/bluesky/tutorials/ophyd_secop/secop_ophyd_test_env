## Demo of SECoP Ophyd Device

This repository contains a Dockerfile which has the environment required to communicate with a SECoP device using Ophyd. 

To run this demo:

1. First run the docker-compose file in the repository https://codebase.helmholtz.cloud/hzb/bluesky/tutorials/ophyd_secop/secop_frappy with `docker-compose up`

2. Now start a container from the image from the Dockerfile in this repo:

    ```
    docker run -it --net=host registry.hzdr.de/hzb/bluesky/tutorials/ophyd_secop/secop_ophyd_test_env:1-0-0 /bin/bash
    ```

3. Attach to this running container with a VS Code Dev container
4. Open the `/opt/cryo_demo.ipynb` file. Install any required extensions
5. Run the notebook. It shows that the device is connected, `readable` and `moveable`. 

![Alt text](image.png)
